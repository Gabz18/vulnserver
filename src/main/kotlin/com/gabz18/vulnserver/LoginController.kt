package com.gabz18.vulnserver

import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/api/v1/auth")
class LoginController {

    @PostMapping("/login")
    fun login(@RequestBody loginRequest: LoginRequest): ResponseEntity<String> {
        if (loginRequest.username == CredentialsHolder.username && loginRequest.password == CredentialsHolder.password) {
            return ResponseEntity.ok().body("Logged in.")
        }
        return ResponseEntity.status(401).body("Unauthorized.")
    }
}