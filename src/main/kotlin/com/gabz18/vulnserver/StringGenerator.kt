package com.gabz18.vulnserver

import kotlin.random.Random

class StringGenerator {

    private val possibleChars = "ABCDEFGHIJKLMOPQRSTUVWXYZabcdefghijklmopqrstuvwxyz0123456789"
    private var currentPossibleCharIndex = 0
    private var generatedString = ""

    fun generateString(length: Int): String {
        val sb = StringBuilder()
        for (i in 0 until length) {
            sb.append(possibleChars.elementAt(Random.nextInt(possibleChars.length)))
        }
        return sb.toString()
    }

    private fun getNext() {
        generatedString[0] =
    }
}