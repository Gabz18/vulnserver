package com.gabz18.vulnserver

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class VulnserverApplication

fun main(args: Array<String>) {

    val stringGenerator = StringGenerator()

    CredentialsHolder.username = stringGenerator.generateString(6)
    CredentialsHolder.password = stringGenerator.generateString(6)

    println("Using generated username : ${CredentialsHolder.username}")
    println("Using generated password : ${CredentialsHolder.password}")

    runApplication<VulnserverApplication>(*args)
}
